#include "xf_dilation_config.h"

static constexpr int __XF_DEPTH = (HEIGHT * WIDTH * (XF_PIXELWIDTH(TYPE, NPC_T)) / 8) / (INPUT_PTR_WIDTH / 8);

void dilation_accel(hls::stream<ap_axiu<8, 1, 1, 1> >& src, hls::stream<ap_axiu<8, 1, 1, 1> >& dst ,/* unsigned char* mkernel, */
                    int height,
                    int width)                   
                     {
// clang-format off
    #pragma HLS INTERFACE axis port=src
    #pragma HLS INTERFACE axis port=dst
    
    #pragma HLS INTERFACE s_axilite port=height              
    #pragma HLS INTERFACE s_axilite port=width       
    #pragma HLS INTERFACE s_axilite port=return   bundle=control
                    
    // clang-format on

    xf::cv::Mat<TYPE, HEIGHT, WIDTH, NPC_T, XF_CV_DEPTH_IN_1> in_mat(height, width);
    xf::cv::Mat<TYPE, HEIGHT, WIDTH, NPC_T, XF_CV_DEPTH_IN_1> out_mat(height, width);


/*
    unsigned char locKernel[FILTER_SIZE * FILTER_SIZE];
    for (int ki = 0; ki < (FILTER_SIZE * FILTER_SIZE); ki++) {
// clang-format off
        #pragma HLS PIPELINE
        // clang-format on
        locKernel[ki] = mkernel[ki];
    }
*/

/*	unsigned char kernel[FILTER_SIZE*FILTER_SIZE] = {0,1,0,
	 	 	 	 	 	         1,1,1,
							 0,1,0
							};
*/	
	unsigned char kernel[FILTER_SIZE*FILTER_SIZE] = {0,0,1,0,0,
 	 	 	 	 	         1,1,1,1,1,
						 1,1,1,1,1,
						 1,1,1,1,1,
						 0,0,1,0,0
						};
								
																				
// clang-format off
    #pragma HLS DATAFLOW
    // clang-format on


    xf::cv::AXIvideo2xfMat(src, in_mat);

    xf::cv::dilate<XF_BORDER_CONSTANT, TYPE, HEIGHT, WIDTH, KERNEL_SHAPE, FILTER_SIZE, FILTER_SIZE, ITERATIONS, NPC_T,  XF_CV_DEPTH_IN_1, XF_CV_DEPTH_OUT_1>(in_mat, out_mat, kernel);
    
    xf::cv::xfMat2AXIvideo(out_mat, dst);
        

}
