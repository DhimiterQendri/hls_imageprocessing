// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xdilation_accel.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XDilation_accel_CfgInitialize(XDilation_accel *InstancePtr, XDilation_accel_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Control_BaseAddress = ConfigPtr->Control_BaseAddress;
    InstancePtr->Control_r_BaseAddress = ConfigPtr->Control_r_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XDilation_accel_Start(XDilation_accel *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDilation_accel_ReadReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_AP_CTRL) & 0x80;
    XDilation_accel_WriteReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_AP_CTRL, Data | 0x01);
}

u32 XDilation_accel_IsDone(XDilation_accel *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDilation_accel_ReadReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XDilation_accel_IsIdle(XDilation_accel *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDilation_accel_ReadReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XDilation_accel_IsReady(XDilation_accel *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDilation_accel_ReadReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XDilation_accel_EnableAutoRestart(XDilation_accel *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDilation_accel_WriteReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_AP_CTRL, 0x80);
}

void XDilation_accel_DisableAutoRestart(XDilation_accel *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDilation_accel_WriteReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_AP_CTRL, 0);
}

void XDilation_accel_Set_height(XDilation_accel *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDilation_accel_WriteReg(InstancePtr->Control_r_BaseAddress, XDILATION_ACCEL_CONTROL_R_ADDR_HEIGHT_DATA, Data);
}

u32 XDilation_accel_Get_height(XDilation_accel *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDilation_accel_ReadReg(InstancePtr->Control_r_BaseAddress, XDILATION_ACCEL_CONTROL_R_ADDR_HEIGHT_DATA);
    return Data;
}

void XDilation_accel_Set_width(XDilation_accel *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDilation_accel_WriteReg(InstancePtr->Control_r_BaseAddress, XDILATION_ACCEL_CONTROL_R_ADDR_WIDTH_DATA, Data);
}

u32 XDilation_accel_Get_width(XDilation_accel *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDilation_accel_ReadReg(InstancePtr->Control_r_BaseAddress, XDILATION_ACCEL_CONTROL_R_ADDR_WIDTH_DATA);
    return Data;
}

void XDilation_accel_InterruptGlobalEnable(XDilation_accel *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDilation_accel_WriteReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_GIE, 1);
}

void XDilation_accel_InterruptGlobalDisable(XDilation_accel *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDilation_accel_WriteReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_GIE, 0);
}

void XDilation_accel_InterruptEnable(XDilation_accel *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XDilation_accel_ReadReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_IER);
    XDilation_accel_WriteReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_IER, Register | Mask);
}

void XDilation_accel_InterruptDisable(XDilation_accel *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XDilation_accel_ReadReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_IER);
    XDilation_accel_WriteReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_IER, Register & (~Mask));
}

void XDilation_accel_InterruptClear(XDilation_accel *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDilation_accel_WriteReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_ISR, Mask);
}

u32 XDilation_accel_InterruptGetEnabled(XDilation_accel *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XDilation_accel_ReadReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_IER);
}

u32 XDilation_accel_InterruptGetStatus(XDilation_accel *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XDilation_accel_ReadReg(InstancePtr->Control_BaseAddress, XDILATION_ACCEL_CONTROL_ADDR_ISR);
}

