// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XDILATION_ACCEL_H
#define XDILATION_ACCEL_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xdilation_accel_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
#else
typedef struct {
    u16 DeviceId;
    u64 Control_BaseAddress;
    u64 Control_r_BaseAddress;
} XDilation_accel_Config;
#endif

typedef struct {
    u64 Control_BaseAddress;
    u64 Control_r_BaseAddress;
    u32 IsReady;
} XDilation_accel;

typedef u32 word_type;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XDilation_accel_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XDilation_accel_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XDilation_accel_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XDilation_accel_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XDilation_accel_Initialize(XDilation_accel *InstancePtr, u16 DeviceId);
XDilation_accel_Config* XDilation_accel_LookupConfig(u16 DeviceId);
int XDilation_accel_CfgInitialize(XDilation_accel *InstancePtr, XDilation_accel_Config *ConfigPtr);
#else
int XDilation_accel_Initialize(XDilation_accel *InstancePtr, const char* InstanceName);
int XDilation_accel_Release(XDilation_accel *InstancePtr);
#endif

void XDilation_accel_Start(XDilation_accel *InstancePtr);
u32 XDilation_accel_IsDone(XDilation_accel *InstancePtr);
u32 XDilation_accel_IsIdle(XDilation_accel *InstancePtr);
u32 XDilation_accel_IsReady(XDilation_accel *InstancePtr);
void XDilation_accel_EnableAutoRestart(XDilation_accel *InstancePtr);
void XDilation_accel_DisableAutoRestart(XDilation_accel *InstancePtr);

void XDilation_accel_Set_height(XDilation_accel *InstancePtr, u32 Data);
u32 XDilation_accel_Get_height(XDilation_accel *InstancePtr);
void XDilation_accel_Set_width(XDilation_accel *InstancePtr, u32 Data);
u32 XDilation_accel_Get_width(XDilation_accel *InstancePtr);

void XDilation_accel_InterruptGlobalEnable(XDilation_accel *InstancePtr);
void XDilation_accel_InterruptGlobalDisable(XDilation_accel *InstancePtr);
void XDilation_accel_InterruptEnable(XDilation_accel *InstancePtr, u32 Mask);
void XDilation_accel_InterruptDisable(XDilation_accel *InstancePtr, u32 Mask);
void XDilation_accel_InterruptClear(XDilation_accel *InstancePtr, u32 Mask);
u32 XDilation_accel_InterruptGetEnabled(XDilation_accel *InstancePtr);
u32 XDilation_accel_InterruptGetStatus(XDilation_accel *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
