// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xdilation_accel.h"

extern XDilation_accel_Config XDilation_accel_ConfigTable[];

XDilation_accel_Config *XDilation_accel_LookupConfig(u16 DeviceId) {
	XDilation_accel_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XDILATION_ACCEL_NUM_INSTANCES; Index++) {
		if (XDilation_accel_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XDilation_accel_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XDilation_accel_Initialize(XDilation_accel *InstancePtr, u16 DeviceId) {
	XDilation_accel_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XDilation_accel_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XDilation_accel_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

