set moduleName dilation_accel
set isTopModule 1
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set hasInterrupt 0
set C_modelName {dilation_accel}
set C_modelType { void 0 }
set C_modelArgList {
	{ src_V_data_V int 8 regular {axi_s 0 volatile  { src Data } }  }
	{ src_V_keep_V int 1 regular {axi_s 0 volatile  { src Keep } }  }
	{ src_V_strb_V int 1 regular {axi_s 0 volatile  { src Strb } }  }
	{ src_V_user_V int 1 regular {axi_s 0 volatile  { src User } }  }
	{ src_V_last_V int 1 regular {axi_s 0 volatile  { src Last } }  }
	{ src_V_id_V int 1 regular {axi_s 0 volatile  { src ID } }  }
	{ src_V_dest_V int 1 regular {axi_s 0 volatile  { src Dest } }  }
	{ dst_V_data_V int 8 regular {axi_s 1 volatile  { dst Data } }  }
	{ dst_V_keep_V int 1 regular {axi_s 1 volatile  { dst Keep } }  }
	{ dst_V_strb_V int 1 regular {axi_s 1 volatile  { dst Strb } }  }
	{ dst_V_user_V int 1 regular {axi_s 1 volatile  { dst User } }  }
	{ dst_V_last_V int 1 regular {axi_s 1 volatile  { dst Last } }  }
	{ dst_V_id_V int 1 regular {axi_s 1 volatile  { dst ID } }  }
	{ dst_V_dest_V int 1 regular {axi_s 1 volatile  { dst Dest } }  }
	{ height int 32 regular {axi_slave 0}  }
	{ width int 32 regular {axi_slave 0}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "src_V_data_V", "interface" : "axis", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "src_V_keep_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "src_V_strb_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "src_V_user_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "src_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "src_V_id_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "src_V_dest_V", "interface" : "axis", "bitwidth" : 1, "direction" : "READONLY"} , 
 	{ "Name" : "dst_V_data_V", "interface" : "axis", "bitwidth" : 8, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dst_V_keep_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dst_V_strb_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dst_V_user_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dst_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dst_V_id_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "dst_V_dest_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY"} , 
 	{ "Name" : "height", "interface" : "axi_slave", "bundle":"control_r","type":"ap_none","bitwidth" : 32, "direction" : "READONLY", "offset" : {"in":16}, "offset_end" : {"in":23}} , 
 	{ "Name" : "width", "interface" : "axi_slave", "bundle":"control_r","type":"ap_none","bitwidth" : 32, "direction" : "READONLY", "offset" : {"in":24}, "offset_end" : {"in":31}} ]}
# RTL Port declarations: 
set portNum 55
set portList { 
	{ s_axi_control_AWVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_AWREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_AWADDR sc_in sc_lv 4 signal -1 } 
	{ s_axi_control_WVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_WREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_WDATA sc_in sc_lv 32 signal -1 } 
	{ s_axi_control_WSTRB sc_in sc_lv 4 signal -1 } 
	{ s_axi_control_ARVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_ARREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_ARADDR sc_in sc_lv 4 signal -1 } 
	{ s_axi_control_RVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_RREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_RDATA sc_out sc_lv 32 signal -1 } 
	{ s_axi_control_RRESP sc_out sc_lv 2 signal -1 } 
	{ s_axi_control_BVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_BREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_BRESP sc_out sc_lv 2 signal -1 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ interrupt sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_r_AWVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_r_AWREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_r_AWADDR sc_in sc_lv 5 signal -1 } 
	{ s_axi_control_r_WVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_r_WREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_r_WDATA sc_in sc_lv 32 signal -1 } 
	{ s_axi_control_r_WSTRB sc_in sc_lv 4 signal -1 } 
	{ s_axi_control_r_ARVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_r_ARREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_r_ARADDR sc_in sc_lv 5 signal -1 } 
	{ s_axi_control_r_RVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_r_RREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_r_RDATA sc_out sc_lv 32 signal -1 } 
	{ s_axi_control_r_RRESP sc_out sc_lv 2 signal -1 } 
	{ s_axi_control_r_BVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_r_BREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_r_BRESP sc_out sc_lv 2 signal -1 } 
	{ src_TDATA sc_in sc_lv 8 signal 0 } 
	{ src_TKEEP sc_in sc_lv 1 signal 1 } 
	{ src_TSTRB sc_in sc_lv 1 signal 2 } 
	{ src_TUSER sc_in sc_lv 1 signal 3 } 
	{ src_TLAST sc_in sc_lv 1 signal 4 } 
	{ src_TID sc_in sc_lv 1 signal 5 } 
	{ src_TDEST sc_in sc_lv 1 signal 6 } 
	{ dst_TDATA sc_out sc_lv 8 signal 7 } 
	{ dst_TKEEP sc_out sc_lv 1 signal 8 } 
	{ dst_TSTRB sc_out sc_lv 1 signal 9 } 
	{ dst_TUSER sc_out sc_lv 1 signal 10 } 
	{ dst_TLAST sc_out sc_lv 1 signal 11 } 
	{ dst_TID sc_out sc_lv 1 signal 12 } 
	{ dst_TDEST sc_out sc_lv 1 signal 13 } 
	{ src_TVALID sc_in sc_logic 1 invld 6 } 
	{ src_TREADY sc_out sc_logic 1 inacc 6 } 
	{ dst_TVALID sc_out sc_logic 1 outvld 13 } 
	{ dst_TREADY sc_in sc_logic 1 outacc 13 } 
}
set NewPortList {[ 
	{ "name": "s_axi_control_AWADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "control", "role": "AWADDR" },"address":[{"name":"dilation_accel","role":"start","value":"0","valid_bit":"0"},{"name":"dilation_accel","role":"continue","value":"0","valid_bit":"4"},{"name":"dilation_accel","role":"auto_start","value":"0","valid_bit":"7"}] },
	{ "name": "s_axi_control_AWVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "AWVALID" } },
	{ "name": "s_axi_control_AWREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "AWREADY" } },
	{ "name": "s_axi_control_WVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "WVALID" } },
	{ "name": "s_axi_control_WREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "WREADY" } },
	{ "name": "s_axi_control_WDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "control", "role": "WDATA" } },
	{ "name": "s_axi_control_WSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "control", "role": "WSTRB" } },
	{ "name": "s_axi_control_ARADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "control", "role": "ARADDR" },"address":[{"name":"dilation_accel","role":"start","value":"0","valid_bit":"0"},{"name":"dilation_accel","role":"done","value":"0","valid_bit":"1"},{"name":"dilation_accel","role":"idle","value":"0","valid_bit":"2"},{"name":"dilation_accel","role":"ready","value":"0","valid_bit":"3"},{"name":"dilation_accel","role":"auto_start","value":"0","valid_bit":"7"}] },
	{ "name": "s_axi_control_ARVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "ARVALID" } },
	{ "name": "s_axi_control_ARREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "ARREADY" } },
	{ "name": "s_axi_control_RVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "RVALID" } },
	{ "name": "s_axi_control_RREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "RREADY" } },
	{ "name": "s_axi_control_RDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "control", "role": "RDATA" } },
	{ "name": "s_axi_control_RRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "control", "role": "RRESP" } },
	{ "name": "s_axi_control_BVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "BVALID" } },
	{ "name": "s_axi_control_BREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "BREADY" } },
	{ "name": "s_axi_control_BRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "control", "role": "BRESP" } },
	{ "name": "interrupt", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "interrupt" } },
	{ "name": "s_axi_control_r_AWADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "control_r", "role": "AWADDR" },"address":[{"name":"height","role":"data","value":"16"},{"name":"width","role":"data","value":"24"}] },
	{ "name": "s_axi_control_r_AWVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "AWVALID" } },
	{ "name": "s_axi_control_r_AWREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "AWREADY" } },
	{ "name": "s_axi_control_r_WVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "WVALID" } },
	{ "name": "s_axi_control_r_WREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "WREADY" } },
	{ "name": "s_axi_control_r_WDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "control_r", "role": "WDATA" } },
	{ "name": "s_axi_control_r_WSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "control_r", "role": "WSTRB" } },
	{ "name": "s_axi_control_r_ARADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "control_r", "role": "ARADDR" },"address":[] },
	{ "name": "s_axi_control_r_ARVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "ARVALID" } },
	{ "name": "s_axi_control_r_ARREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "ARREADY" } },
	{ "name": "s_axi_control_r_RVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "RVALID" } },
	{ "name": "s_axi_control_r_RREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "RREADY" } },
	{ "name": "s_axi_control_r_RDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "control_r", "role": "RDATA" } },
	{ "name": "s_axi_control_r_RRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "control_r", "role": "RRESP" } },
	{ "name": "s_axi_control_r_BVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "BVALID" } },
	{ "name": "s_axi_control_r_BREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control_r", "role": "BREADY" } },
	{ "name": "s_axi_control_r_BRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "control_r", "role": "BRESP" } }, 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "src_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "src_V_data_V", "role": "default" }} , 
 	{ "name": "src_TKEEP", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "src_V_keep_V", "role": "default" }} , 
 	{ "name": "src_TSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "src_V_strb_V", "role": "default" }} , 
 	{ "name": "src_TUSER", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "src_V_user_V", "role": "default" }} , 
 	{ "name": "src_TLAST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "src_V_last_V", "role": "default" }} , 
 	{ "name": "src_TID", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "src_V_id_V", "role": "default" }} , 
 	{ "name": "src_TDEST", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "src_V_dest_V", "role": "default" }} , 
 	{ "name": "dst_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "dst_V_data_V", "role": "default" }} , 
 	{ "name": "dst_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dst_V_keep_V", "role": "default" }} , 
 	{ "name": "dst_TSTRB", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dst_V_strb_V", "role": "default" }} , 
 	{ "name": "dst_TUSER", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dst_V_user_V", "role": "default" }} , 
 	{ "name": "dst_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dst_V_last_V", "role": "default" }} , 
 	{ "name": "dst_TID", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dst_V_id_V", "role": "default" }} , 
 	{ "name": "dst_TDEST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dst_V_dest_V", "role": "default" }} , 
 	{ "name": "src_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "src_V_dest_V", "role": "default" }} , 
 	{ "name": "src_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "src_V_dest_V", "role": "default" }} , 
 	{ "name": "dst_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "dst_V_dest_V", "role": "default" }} , 
 	{ "name": "dst_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "dst_V_dest_V", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "18", "43", "53", "54", "55", "56", "57", "58", "59", "60", "61"],
		"CDFG" : "dilation_accel",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "53", "EstimateLatencyMax" : "19747",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "1",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"InputProcess" : [
			{"ID" : "3", "Name" : "Block_entry1_proc_U0"}],
		"OutputProcess" : [
			{"ID" : "43", "Name" : "xfMat2AXIvideo_8_0_128_128_1_1_U0"}],
		"Port" : [
			{"Name" : "src_V_data_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "AXIvideo2xfMat_8_0_128_128_1_1_U0", "Port" : "src_V_data_V"}]},
			{"Name" : "src_V_keep_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "AXIvideo2xfMat_8_0_128_128_1_1_U0", "Port" : "src_V_keep_V"}]},
			{"Name" : "src_V_strb_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "AXIvideo2xfMat_8_0_128_128_1_1_U0", "Port" : "src_V_strb_V"}]},
			{"Name" : "src_V_user_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "AXIvideo2xfMat_8_0_128_128_1_1_U0", "Port" : "src_V_user_V"}]},
			{"Name" : "src_V_last_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "AXIvideo2xfMat_8_0_128_128_1_1_U0", "Port" : "src_V_last_V"}]},
			{"Name" : "src_V_id_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "AXIvideo2xfMat_8_0_128_128_1_1_U0", "Port" : "src_V_id_V"}]},
			{"Name" : "src_V_dest_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "4", "SubInstance" : "AXIvideo2xfMat_8_0_128_128_1_1_U0", "Port" : "src_V_dest_V"}]},
			{"Name" : "dst_V_data_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "43", "SubInstance" : "xfMat2AXIvideo_8_0_128_128_1_1_U0", "Port" : "dst_V_data_V"}]},
			{"Name" : "dst_V_keep_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "43", "SubInstance" : "xfMat2AXIvideo_8_0_128_128_1_1_U0", "Port" : "dst_V_keep_V"}]},
			{"Name" : "dst_V_strb_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "43", "SubInstance" : "xfMat2AXIvideo_8_0_128_128_1_1_U0", "Port" : "dst_V_strb_V"}]},
			{"Name" : "dst_V_user_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "43", "SubInstance" : "xfMat2AXIvideo_8_0_128_128_1_1_U0", "Port" : "dst_V_user_V"}]},
			{"Name" : "dst_V_last_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "43", "SubInstance" : "xfMat2AXIvideo_8_0_128_128_1_1_U0", "Port" : "dst_V_last_V"}]},
			{"Name" : "dst_V_id_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "43", "SubInstance" : "xfMat2AXIvideo_8_0_128_128_1_1_U0", "Port" : "dst_V_id_V"}]},
			{"Name" : "dst_V_dest_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "43", "SubInstance" : "xfMat2AXIvideo_8_0_128_128_1_1_U0", "Port" : "dst_V_dest_V"}]},
			{"Name" : "height", "Type" : "None", "Direction" : "I"},
			{"Name" : "width", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.control_s_axi_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.control_r_s_axi_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.Block_entry1_proc_U0", "Parent" : "0",
		"CDFG" : "Block_entry1_proc",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "height", "Type" : "None", "Direction" : "I"},
			{"Name" : "width", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0", "Parent" : "0", "Child" : ["5", "7", "9", "11", "12", "13", "14", "15", "16", "17"],
		"CDFG" : "AXIvideo2xfMat_8_0_128_128_1_1_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1", "real_start" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "6", "EstimateLatencyMax" : "17798",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "src_V_data_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt_fu_185", "Port" : "src_V_data_V", "Inst_start_state" : "2", "Inst_end_state" : "3"},
					{"ID" : "7", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205", "Port" : "src_V_data_V", "Inst_start_state" : "5", "Inst_end_state" : "6"},
					{"ID" : "9", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt_fu_232", "Port" : "src_V_data_V", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "src_V_keep_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt_fu_185", "Port" : "src_V_keep_V", "Inst_start_state" : "2", "Inst_end_state" : "3"},
					{"ID" : "7", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205", "Port" : "src_V_keep_V", "Inst_start_state" : "5", "Inst_end_state" : "6"},
					{"ID" : "9", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt_fu_232", "Port" : "src_V_keep_V", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "src_V_strb_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt_fu_185", "Port" : "src_V_strb_V", "Inst_start_state" : "2", "Inst_end_state" : "3"},
					{"ID" : "7", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205", "Port" : "src_V_strb_V", "Inst_start_state" : "5", "Inst_end_state" : "6"},
					{"ID" : "9", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt_fu_232", "Port" : "src_V_strb_V", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "src_V_user_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt_fu_185", "Port" : "src_V_user_V", "Inst_start_state" : "2", "Inst_end_state" : "3"},
					{"ID" : "7", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205", "Port" : "src_V_user_V", "Inst_start_state" : "5", "Inst_end_state" : "6"},
					{"ID" : "9", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt_fu_232", "Port" : "src_V_user_V", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "src_V_last_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt_fu_185", "Port" : "src_V_last_V", "Inst_start_state" : "2", "Inst_end_state" : "3"},
					{"ID" : "7", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205", "Port" : "src_V_last_V", "Inst_start_state" : "5", "Inst_end_state" : "6"},
					{"ID" : "9", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt_fu_232", "Port" : "src_V_last_V", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "src_V_id_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt_fu_185", "Port" : "src_V_id_V", "Inst_start_state" : "2", "Inst_end_state" : "3"},
					{"ID" : "7", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205", "Port" : "src_V_id_V", "Inst_start_state" : "5", "Inst_end_state" : "6"},
					{"ID" : "9", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt_fu_232", "Port" : "src_V_id_V", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "src_V_dest_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"SubConnect" : [
					{"ID" : "5", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt_fu_185", "Port" : "src_V_dest_V", "Inst_start_state" : "2", "Inst_end_state" : "3"},
					{"ID" : "7", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205", "Port" : "src_V_dest_V", "Inst_start_state" : "5", "Inst_end_state" : "6"},
					{"ID" : "9", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt_fu_232", "Port" : "src_V_dest_V", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "p_read", "Type" : "None", "Direction" : "I", "DependentProc" : ["3"], "DependentChan" : "55", "DependentChanDepth" : "2", "DependentChanType" : "1"},
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I", "DependentProc" : ["3"], "DependentChan" : "56", "DependentChanDepth" : "2", "DependentChanType" : "1"},
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "O", "DependentProc" : ["18"], "DependentChan" : "57", "DependentChanDepth" : "1", "DependentChanType" : "0",
				"SubConnect" : [
					{"ID" : "7", "SubInstance" : "grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205", "Port" : "in_mat_data24", "Inst_start_state" : "5", "Inst_end_state" : "6"}]},
			{"Name" : "in_mat_rows_c", "Type" : "Fifo", "Direction" : "O", "DependentProc" : ["18"], "DependentChan" : "58", "DependentChanDepth" : "2", "DependentChanType" : "2",
				"BlockSignal" : [
					{"Name" : "in_mat_rows_c_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "in_mat_cols_c", "Type" : "Fifo", "Direction" : "O", "DependentProc" : ["18"], "DependentChan" : "59", "DependentChanDepth" : "2", "DependentChanType" : "2",
				"BlockSignal" : [
					{"Name" : "in_mat_cols_c_blk_n", "Type" : "RtlSignal"}]}],
		"Loop" : [
			{"Name" : "loop_row_axi2mat", "PipelineType" : "no",
				"LoopDec" : {"FSMBitwidth" : "10", "FirstState" : "ap_ST_fsm_state5", "LastState" : ["ap_ST_fsm_state10"], "QuitState" : ["ap_ST_fsm_state5"], "PreState" : ["ap_ST_fsm_state4"], "PostState" : ["ap_ST_fsm_state1"], "OneDepthLoop" : "0", "OneStateBlock": ""}}]},
	{"ID" : "5", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt_fu_185", "Parent" : "4", "Child" : ["6"],
		"CDFG" : "AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "2",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "src_V_data_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"BlockSignal" : [
					{"Name" : "src_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "src_V_keep_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_strb_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_user_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_last_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_id_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_dest_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "axi_last_V_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "axi_data_V_out", "Type" : "Vld", "Direction" : "O"}],
		"Loop" : [
			{"Name" : "loop_start_hunt", "PipelineType" : "NotSupport"}]},
	{"ID" : "6", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt_fu_185.flow_control_loop_pipe_sequential_init_U", "Parent" : "5"},
	{"ID" : "7", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205", "Parent" : "4", "Child" : ["8"],
		"CDFG" : "AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "3", "EstimateLatencyMax" : "130",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "start_2", "Type" : "None", "Direction" : "I"},
			{"Name" : "axi_data_V_2", "Type" : "None", "Direction" : "I"},
			{"Name" : "axi_last_V_2", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"},
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "in_mat_data24_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "src_V_data_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"BlockSignal" : [
					{"Name" : "src_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "src_V_keep_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_strb_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_user_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_last_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_id_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_dest_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "last_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "axi_data_V_3_out", "Type" : "Vld", "Direction" : "O"}],
		"Loop" : [
			{"Name" : "loop_col_zxi2mat", "PipelineType" : "UPC",
				"LoopDec" : {"FSMBitwidth" : "1", "FirstState" : "ap_ST_fsm_pp0_stage0", "FirstStateIter" : "ap_enable_reg_pp0_iter0", "FirstStateBlock" : "ap_block_pp0_stage0_subdone", "LastState" : "ap_ST_fsm_pp0_stage0", "LastStateIter" : "ap_enable_reg_pp0_iter1", "LastStateBlock" : "ap_block_pp0_stage0_subdone", "QuitState" : "ap_ST_fsm_pp0_stage0", "QuitStateIter" : "ap_enable_reg_pp0_iter0", "QuitStateBlock" : "ap_block_pp0_stage0_subdone", "OneDepthLoop" : "0", "has_ap_ctrl" : "1", "has_continue" : "0"}}]},
	{"ID" : "8", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat_fu_205.flow_control_loop_pipe_sequential_init_U", "Parent" : "7"},
	{"ID" : "9", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt_fu_232", "Parent" : "4", "Child" : ["10"],
		"CDFG" : "AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "3",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "axi_last_2_lcssa", "Type" : "None", "Direction" : "I"},
			{"Name" : "axi_data_2_lcssa", "Type" : "None", "Direction" : "I"},
			{"Name" : "last_0_lcssa", "Type" : "None", "Direction" : "I"},
			{"Name" : "src_V_data_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src",
				"BlockSignal" : [
					{"Name" : "src_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "src_V_keep_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_strb_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_user_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_last_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_id_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "src_V_dest_V", "Type" : "Axis", "Direction" : "I", "BaseName" : "src"},
			{"Name" : "axi_last_V_4_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "axi_data_V_4_out", "Type" : "Vld", "Direction" : "O"}],
		"Loop" : [
			{"Name" : "loop_last_hunt", "PipelineType" : "NotSupport"}]},
	{"ID" : "10", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.grp_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt_fu_232.flow_control_loop_pipe_sequential_init_U", "Parent" : "9"},
	{"ID" : "11", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.regslice_both_src_V_data_V_U", "Parent" : "4"},
	{"ID" : "12", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.regslice_both_src_V_keep_V_U", "Parent" : "4"},
	{"ID" : "13", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.regslice_both_src_V_strb_V_U", "Parent" : "4"},
	{"ID" : "14", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.regslice_both_src_V_user_V_U", "Parent" : "4"},
	{"ID" : "15", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.regslice_both_src_V_last_V_U", "Parent" : "4"},
	{"ID" : "16", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.regslice_both_src_V_id_V_U", "Parent" : "4"},
	{"ID" : "17", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.AXIvideo2xfMat_8_0_128_128_1_1_U0.regslice_both_src_V_dest_V_U", "Parent" : "4"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0", "Parent" : "0", "Child" : ["19"],
		"CDFG" : "dilate_0_0_128_128_2_5_5_1_1_1_1_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "51", "EstimateLatencyMax" : "19745",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"StartSource" : "4",
		"StartFifo" : "start_for_dilate_0_0_128_128_2_5_5_1_1_1_1_U0_U",
		"Port" : [
			{"Name" : "p_src_rows", "Type" : "Fifo", "Direction" : "I", "DependentProc" : ["4"], "DependentChan" : "58", "DependentChanDepth" : "2", "DependentChanType" : "2",
				"BlockSignal" : [
					{"Name" : "p_src_rows_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "p_src_cols", "Type" : "Fifo", "Direction" : "I", "DependentProc" : ["4"], "DependentChan" : "59", "DependentChanDepth" : "2", "DependentChanType" : "2",
				"BlockSignal" : [
					{"Name" : "p_src_cols_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "I", "DependentProc" : ["4"], "DependentChan" : "57", "DependentChanDepth" : "1", "DependentChanType" : "0",
				"SubConnect" : [
					{"ID" : "19", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44", "Port" : "in_mat_data24", "Inst_start_state" : "1", "Inst_end_state" : "2"}]},
			{"Name" : "out_mat_data25", "Type" : "Fifo", "Direction" : "O", "DependentProc" : ["43"], "DependentChan" : "60", "DependentChanDepth" : "1", "DependentChanType" : "0",
				"SubConnect" : [
					{"ID" : "19", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44", "Port" : "out_mat_data25", "Inst_start_state" : "1", "Inst_end_state" : "2"}]}]},
	{"ID" : "19", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44", "Parent" : "18", "Child" : ["20", "21", "22", "23", "24", "25", "27", "29", "31"],
		"CDFG" : "xfdilate_128_128_1_0_1_1_1_0_130_5_5_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "50", "EstimateLatencyMax" : "19744",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "27", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2_fu_163", "Port" : "in_mat_data24", "Inst_start_state" : "4", "Inst_end_state" : "5"},
					{"ID" : "31", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181", "Port" : "in_mat_data24", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "out_mat_data25", "Type" : "Fifo", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "31", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181", "Port" : "out_mat_data25", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "img_height", "Type" : "None", "Direction" : "I"},
			{"Name" : "img_width", "Type" : "None", "Direction" : "I"}],
		"Loop" : [
			{"Name" : "read_lines", "PipelineType" : "no",
				"LoopDec" : {"FSMBitwidth" : "9", "FirstState" : "ap_ST_fsm_state4", "LastState" : ["ap_ST_fsm_state5"], "QuitState" : ["ap_ST_fsm_state4"], "PreState" : ["ap_ST_fsm_state3"], "PostState" : ["ap_ST_fsm_state6"], "OneDepthLoop" : "0", "OneStateBlock": ""}},
			{"Name" : "Row_Loop", "PipelineType" : "no",
				"LoopDec" : {"FSMBitwidth" : "9", "FirstState" : "ap_ST_fsm_state7", "LastState" : ["ap_ST_fsm_state9"], "QuitState" : ["ap_ST_fsm_state7"], "PreState" : ["ap_ST_fsm_state6"], "PostState" : ["ap_ST_fsm_state1"], "OneDepthLoop" : "0", "OneStateBlock": ""}}]},
	{"ID" : "20", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_U", "Parent" : "19"},
	{"ID" : "21", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_1_U", "Parent" : "19"},
	{"ID" : "22", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_2_U", "Parent" : "19"},
	{"ID" : "23", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_3_U", "Parent" : "19"},
	{"ID" : "24", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_4_U", "Parent" : "19"},
	{"ID" : "25", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1_fu_154", "Parent" : "19", "Child" : ["26"],
		"CDFG" : "xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "7", "EstimateLatencyMax" : "7",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "row_ind_V_4_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "row_ind_V_3_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "row_ind_V_2_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "row_ind_V_1_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "row_ind_V_out", "Type" : "Vld", "Direction" : "O"}],
		"Loop" : [
			{"Name" : "VITIS_LOOP_279_1", "PipelineType" : "NotSupport"}]},
	{"ID" : "26", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1_fu_154.flow_control_loop_pipe_sequential_init_U", "Parent" : "25"},
	{"ID" : "27", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2_fu_163", "Parent" : "19", "Child" : ["28"],
		"CDFG" : "xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "133",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "img_width", "Type" : "None", "Direction" : "I"},
			{"Name" : "buf_V_3", "Type" : "Memory", "Direction" : "O"},
			{"Name" : "buf_V_2", "Type" : "Memory", "Direction" : "O"},
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "in_mat_data24_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cond21", "Type" : "None", "Direction" : "I"}],
		"Loop" : [
			{"Name" : "VITIS_LOOP_294_2", "PipelineType" : "UPC",
				"LoopDec" : {"FSMBitwidth" : "1", "FirstState" : "ap_ST_fsm_pp0_stage0", "FirstStateIter" : "ap_enable_reg_pp0_iter0", "FirstStateBlock" : "ap_block_pp0_stage0_subdone", "LastState" : "ap_ST_fsm_pp0_stage0", "LastStateIter" : "ap_enable_reg_pp0_iter2", "LastStateBlock" : "ap_block_pp0_stage0_subdone", "QuitState" : "ap_ST_fsm_pp0_stage0", "QuitStateIter" : "ap_enable_reg_pp0_iter2", "QuitStateBlock" : "ap_block_pp0_stage0_subdone", "OneDepthLoop" : "0", "has_ap_ctrl" : "1", "has_continue" : "0"}}]},
	{"ID" : "28", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2_fu_163.flow_control_loop_pipe_sequential_init_U", "Parent" : "27"},
	{"ID" : "29", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3_fu_173", "Parent" : "19", "Child" : ["30"],
		"CDFG" : "xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "262",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "img_width", "Type" : "None", "Direction" : "I"},
			{"Name" : "zext_ln1027_6", "Type" : "None", "Direction" : "I"},
			{"Name" : "buf_V", "Type" : "Memory", "Direction" : "O"},
			{"Name" : "buf_V_1", "Type" : "Memory", "Direction" : "O"}],
		"Loop" : [
			{"Name" : "init_boarder_VITIS_LOOP_311_3", "PipelineType" : "UPC",
				"LoopDec" : {"FSMBitwidth" : "1", "FirstState" : "ap_ST_fsm_pp0_stage0", "FirstStateIter" : "ap_enable_reg_pp0_iter0", "FirstStateBlock" : "ap_block_pp0_stage0_subdone", "LastState" : "ap_ST_fsm_pp0_stage0", "LastStateIter" : "ap_enable_reg_pp0_iter1", "LastStateBlock" : "ap_block_pp0_stage0_subdone", "QuitState" : "ap_ST_fsm_pp0_stage0", "QuitStateIter" : "ap_enable_reg_pp0_iter0", "QuitStateBlock" : "ap_block_pp0_stage0_subdone", "OneDepthLoop" : "0", "has_ap_ctrl" : "1", "has_continue" : "0"}}]},
	{"ID" : "30", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3_fu_173.flow_control_loop_pipe_sequential_init_U", "Parent" : "29"},
	{"ID" : "31", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181", "Parent" : "19", "Child" : ["32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42"],
		"CDFG" : "xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "19", "EstimateLatencyMax" : "147",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "op2_assign_1", "Type" : "None", "Direction" : "I"},
			{"Name" : "buf_V_4", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "buf_V_3", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "buf_V_2", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "buf_V_1", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "buf_V", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "img_width", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast1", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast2", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast3", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast4", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast", "Type" : "None", "Direction" : "I"},
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "in_mat_data24_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cmp_i_i131_i", "Type" : "None", "Direction" : "I"},
			{"Name" : "out_mat_data25", "Type" : "Fifo", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "out_mat_data25_blk_n", "Type" : "RtlSignal"}]}],
		"Loop" : [
			{"Name" : "Col_Loop", "PipelineType" : "UPC",
				"LoopDec" : {"FSMBitwidth" : "1", "FirstState" : "ap_ST_fsm_pp0_stage0", "FirstStateIter" : "ap_enable_reg_pp0_iter0", "FirstStateBlock" : "ap_block_pp0_stage0_subdone", "LastState" : "ap_ST_fsm_pp0_stage0", "LastStateIter" : "ap_enable_reg_pp0_iter16", "LastStateBlock" : "ap_block_pp0_stage0_subdone", "QuitState" : "ap_ST_fsm_pp0_stage0", "QuitStateIter" : "ap_enable_reg_pp0_iter16", "QuitStateBlock" : "ap_block_pp0_stage0_subdone", "OneDepthLoop" : "0", "has_ap_ctrl" : "1", "has_continue" : "0"}}]},
	{"ID" : "32", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_xfExtractPixels_1_1_0_s_fu_376", "Parent" : "31",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "33", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_5_xfExtractPixels_1_1_0_s_fu_382", "Parent" : "31",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "34", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_8_xfExtractPixels_1_1_0_s_fu_388", "Parent" : "31",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "35", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_6_xfExtractPixels_1_1_0_s_fu_394", "Parent" : "31",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "36", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_7_xfExtractPixels_1_1_0_s_fu_400", "Parent" : "31",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "37", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U72", "Parent" : "31"},
	{"ID" : "38", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U73", "Parent" : "31"},
	{"ID" : "39", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U74", "Parent" : "31"},
	{"ID" : "40", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U75", "Parent" : "31"},
	{"ID" : "41", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U76", "Parent" : "31"},
	{"ID" : "42", "Level" : "4", "Path" : "`AUTOTB_DUT_INST.dilate_0_0_128_128_2_5_5_1_1_1_1_U0.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.flow_control_loop_pipe_sequential_init_U", "Parent" : "31"},
	{"ID" : "43", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0", "Parent" : "0", "Child" : ["44", "46", "47", "48", "49", "50", "51", "52"],
		"CDFG" : "xfMat2AXIvideo_8_0_128_128_1_1_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "2", "EstimateLatencyMax" : "17026",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read", "Type" : "None", "Direction" : "I", "DependentProc" : ["3"], "DependentChan" : "53", "DependentChanDepth" : "4", "DependentChanType" : "1"},
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I", "DependentProc" : ["3"], "DependentChan" : "54", "DependentChanDepth" : "4", "DependentChanType" : "1"},
			{"Name" : "out_mat_data25", "Type" : "Fifo", "Direction" : "I", "DependentProc" : ["18"], "DependentChan" : "60", "DependentChanDepth" : "1", "DependentChanType" : "0",
				"SubConnect" : [
					{"ID" : "44", "SubInstance" : "grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86", "Port" : "out_mat_data25", "Inst_start_state" : "2", "Inst_end_state" : "3"}]},
			{"Name" : "dst_V_data_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "44", "SubInstance" : "grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86", "Port" : "dst_V_data_V", "Inst_start_state" : "2", "Inst_end_state" : "3"}]},
			{"Name" : "dst_V_keep_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "44", "SubInstance" : "grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86", "Port" : "dst_V_keep_V", "Inst_start_state" : "2", "Inst_end_state" : "3"}]},
			{"Name" : "dst_V_strb_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "44", "SubInstance" : "grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86", "Port" : "dst_V_strb_V", "Inst_start_state" : "2", "Inst_end_state" : "3"}]},
			{"Name" : "dst_V_user_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "44", "SubInstance" : "grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86", "Port" : "dst_V_user_V", "Inst_start_state" : "2", "Inst_end_state" : "3"}]},
			{"Name" : "dst_V_last_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "44", "SubInstance" : "grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86", "Port" : "dst_V_last_V", "Inst_start_state" : "2", "Inst_end_state" : "3"}]},
			{"Name" : "dst_V_id_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "44", "SubInstance" : "grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86", "Port" : "dst_V_id_V", "Inst_start_state" : "2", "Inst_end_state" : "3"}]},
			{"Name" : "dst_V_dest_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"SubConnect" : [
					{"ID" : "44", "SubInstance" : "grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86", "Port" : "dst_V_dest_V", "Inst_start_state" : "2", "Inst_end_state" : "3"}]}],
		"Loop" : [
			{"Name" : "loop_row_mat2axi", "PipelineType" : "no",
				"LoopDec" : {"FSMBitwidth" : "4", "FirstState" : "ap_ST_fsm_state2", "LastState" : ["ap_ST_fsm_state3"], "QuitState" : ["ap_ST_fsm_state2"], "PreState" : ["ap_ST_fsm_state1"], "PostState" : ["ap_ST_fsm_state4"], "OneDepthLoop" : "0", "OneStateBlock": ""}}]},
	{"ID" : "44", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0.grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86", "Parent" : "43", "Child" : ["45"],
		"CDFG" : "xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "131",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "sof", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"},
			{"Name" : "sub", "Type" : "None", "Direction" : "I"},
			{"Name" : "out_mat_data25", "Type" : "Fifo", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "out_mat_data25_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dst_V_data_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst",
				"BlockSignal" : [
					{"Name" : "dst_TDATA_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dst_V_keep_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst"},
			{"Name" : "dst_V_strb_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst"},
			{"Name" : "dst_V_user_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst"},
			{"Name" : "dst_V_last_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst"},
			{"Name" : "dst_V_id_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst"},
			{"Name" : "dst_V_dest_V", "Type" : "Axis", "Direction" : "O", "BaseName" : "dst"}],
		"Loop" : [
			{"Name" : "loop_col_mat2axi", "PipelineType" : "UPC",
				"LoopDec" : {"FSMBitwidth" : "1", "FirstState" : "ap_ST_fsm_pp0_stage0", "FirstStateIter" : "ap_enable_reg_pp0_iter0", "FirstStateBlock" : "ap_block_pp0_stage0_subdone", "LastState" : "ap_ST_fsm_pp0_stage0", "LastStateIter" : "ap_enable_reg_pp0_iter1", "LastStateBlock" : "ap_block_pp0_stage0_subdone", "QuitState" : "ap_ST_fsm_pp0_stage0", "QuitStateIter" : "ap_enable_reg_pp0_iter1", "QuitStateBlock" : "ap_block_pp0_stage0_subdone", "OneDepthLoop" : "0", "has_ap_ctrl" : "1", "has_continue" : "0"}}]},
	{"ID" : "45", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0.grp_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi_fu_86.flow_control_loop_pipe_sequential_init_U", "Parent" : "44"},
	{"ID" : "46", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0.regslice_both_dst_V_data_V_U", "Parent" : "43"},
	{"ID" : "47", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0.regslice_both_dst_V_keep_V_U", "Parent" : "43"},
	{"ID" : "48", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0.regslice_both_dst_V_strb_V_U", "Parent" : "43"},
	{"ID" : "49", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0.regslice_both_dst_V_user_V_U", "Parent" : "43"},
	{"ID" : "50", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0.regslice_both_dst_V_last_V_U", "Parent" : "43"},
	{"ID" : "51", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0.regslice_both_dst_V_id_V_U", "Parent" : "43"},
	{"ID" : "52", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.xfMat2AXIvideo_8_0_128_128_1_1_U0.regslice_both_dst_V_dest_V_U", "Parent" : "43"},
	{"ID" : "53", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.out_mat_rows_channel_U", "Parent" : "0"},
	{"ID" : "54", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.out_mat_cols_channel_U", "Parent" : "0"},
	{"ID" : "55", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.in_mat_rows_c9_channel_U", "Parent" : "0"},
	{"ID" : "56", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.in_mat_cols_c10_channel_U", "Parent" : "0"},
	{"ID" : "57", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.in_mat_data_U", "Parent" : "0"},
	{"ID" : "58", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.in_mat_rows_c_U", "Parent" : "0"},
	{"ID" : "59", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.in_mat_cols_c_U", "Parent" : "0"},
	{"ID" : "60", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.out_mat_data_U", "Parent" : "0"},
	{"ID" : "61", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.start_for_dilate_0_0_128_128_2_5_5_1_1_1_1_U0_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	dilation_accel {
		src_V_data_V {Type I LastRead 0 FirstWrite -1}
		src_V_keep_V {Type I LastRead 0 FirstWrite -1}
		src_V_strb_V {Type I LastRead 0 FirstWrite -1}
		src_V_user_V {Type I LastRead 0 FirstWrite -1}
		src_V_last_V {Type I LastRead 0 FirstWrite -1}
		src_V_id_V {Type I LastRead 0 FirstWrite -1}
		src_V_dest_V {Type I LastRead 0 FirstWrite -1}
		dst_V_data_V {Type O LastRead -1 FirstWrite 1}
		dst_V_keep_V {Type O LastRead -1 FirstWrite 1}
		dst_V_strb_V {Type O LastRead -1 FirstWrite 1}
		dst_V_user_V {Type O LastRead -1 FirstWrite 1}
		dst_V_last_V {Type O LastRead -1 FirstWrite 1}
		dst_V_id_V {Type O LastRead -1 FirstWrite 1}
		dst_V_dest_V {Type O LastRead -1 FirstWrite 1}
		height {Type I LastRead 0 FirstWrite -1}
		width {Type I LastRead 0 FirstWrite -1}}
	Block_entry1_proc {
		height {Type I LastRead 0 FirstWrite -1}
		width {Type I LastRead 0 FirstWrite -1}}
	AXIvideo2xfMat_8_0_128_128_1_1_s {
		src_V_data_V {Type I LastRead 0 FirstWrite -1}
		src_V_keep_V {Type I LastRead 0 FirstWrite -1}
		src_V_strb_V {Type I LastRead 0 FirstWrite -1}
		src_V_user_V {Type I LastRead 0 FirstWrite -1}
		src_V_last_V {Type I LastRead 0 FirstWrite -1}
		src_V_id_V {Type I LastRead 0 FirstWrite -1}
		src_V_dest_V {Type I LastRead 0 FirstWrite -1}
		p_read {Type I LastRead 0 FirstWrite -1}
		p_read1 {Type I LastRead 0 FirstWrite -1}
		in_mat_data24 {Type O LastRead -1 FirstWrite 1}
		in_mat_rows_c {Type O LastRead -1 FirstWrite 0}
		in_mat_cols_c {Type O LastRead -1 FirstWrite 0}}
	AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt {
		src_V_data_V {Type I LastRead 0 FirstWrite -1}
		src_V_keep_V {Type I LastRead 0 FirstWrite -1}
		src_V_strb_V {Type I LastRead 0 FirstWrite -1}
		src_V_user_V {Type I LastRead 0 FirstWrite -1}
		src_V_last_V {Type I LastRead 0 FirstWrite -1}
		src_V_id_V {Type I LastRead 0 FirstWrite -1}
		src_V_dest_V {Type I LastRead 0 FirstWrite -1}
		axi_last_V_out {Type O LastRead -1 FirstWrite 0}
		axi_data_V_out {Type O LastRead -1 FirstWrite 0}}
	AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat {
		start_2 {Type I LastRead 0 FirstWrite -1}
		axi_data_V_2 {Type I LastRead 0 FirstWrite -1}
		axi_last_V_2 {Type I LastRead 0 FirstWrite -1}
		p_read1 {Type I LastRead 0 FirstWrite -1}
		in_mat_data24 {Type O LastRead -1 FirstWrite 1}
		src_V_data_V {Type I LastRead 0 FirstWrite -1}
		src_V_keep_V {Type I LastRead 0 FirstWrite -1}
		src_V_strb_V {Type I LastRead 0 FirstWrite -1}
		src_V_user_V {Type I LastRead 0 FirstWrite -1}
		src_V_last_V {Type I LastRead 0 FirstWrite -1}
		src_V_id_V {Type I LastRead 0 FirstWrite -1}
		src_V_dest_V {Type I LastRead 0 FirstWrite -1}
		last_out {Type O LastRead -1 FirstWrite 0}
		axi_data_V_3_out {Type O LastRead -1 FirstWrite 0}}
	AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt {
		axi_last_2_lcssa {Type I LastRead 0 FirstWrite -1}
		axi_data_2_lcssa {Type I LastRead 0 FirstWrite -1}
		last_0_lcssa {Type I LastRead 0 FirstWrite -1}
		src_V_data_V {Type I LastRead 0 FirstWrite -1}
		src_V_keep_V {Type I LastRead 0 FirstWrite -1}
		src_V_strb_V {Type I LastRead 0 FirstWrite -1}
		src_V_user_V {Type I LastRead 0 FirstWrite -1}
		src_V_last_V {Type I LastRead 0 FirstWrite -1}
		src_V_id_V {Type I LastRead 0 FirstWrite -1}
		src_V_dest_V {Type I LastRead 0 FirstWrite -1}
		axi_last_V_4_out {Type O LastRead -1 FirstWrite 0}
		axi_data_V_4_out {Type O LastRead -1 FirstWrite 0}}
	dilate_0_0_128_128_2_5_5_1_1_1_1_s {
		p_src_rows {Type I LastRead 0 FirstWrite -1}
		p_src_cols {Type I LastRead 0 FirstWrite -1}
		in_mat_data24 {Type I LastRead 1 FirstWrite -1}
		out_mat_data25 {Type O LastRead -1 FirstWrite 16}}
	xfdilate_128_128_1_0_1_1_1_0_130_5_5_s {
		in_mat_data24 {Type I LastRead 1 FirstWrite -1}
		out_mat_data25 {Type O LastRead -1 FirstWrite 16}
		img_height {Type I LastRead 2 FirstWrite -1}
		img_width {Type I LastRead 2 FirstWrite -1}}
	xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1 {
		row_ind_V_4_out {Type O LastRead -1 FirstWrite 0}
		row_ind_V_3_out {Type O LastRead -1 FirstWrite 0}
		row_ind_V_2_out {Type O LastRead -1 FirstWrite 0}
		row_ind_V_1_out {Type O LastRead -1 FirstWrite 0}
		row_ind_V_out {Type O LastRead -1 FirstWrite 0}}
	xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2 {
		img_width {Type I LastRead 0 FirstWrite -1}
		buf_V_3 {Type O LastRead -1 FirstWrite 2}
		buf_V_2 {Type O LastRead -1 FirstWrite 2}
		in_mat_data24 {Type I LastRead 1 FirstWrite -1}
		cond21 {Type I LastRead 0 FirstWrite -1}}
	xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3 {
		img_width {Type I LastRead 0 FirstWrite -1}
		zext_ln1027_6 {Type I LastRead 0 FirstWrite -1}
		buf_V {Type O LastRead -1 FirstWrite 1}
		buf_V_1 {Type O LastRead -1 FirstWrite 1}}
	xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop {
		op2_assign_1 {Type I LastRead 0 FirstWrite -1}
		buf_V_4 {Type IO LastRead 3 FirstWrite 2}
		buf_V_3 {Type IO LastRead 3 FirstWrite 2}
		buf_V_2 {Type IO LastRead 3 FirstWrite 2}
		buf_V_1 {Type IO LastRead 3 FirstWrite 2}
		buf_V {Type IO LastRead 3 FirstWrite 2}
		img_width {Type I LastRead 0 FirstWrite -1}
		p_cast1 {Type I LastRead 0 FirstWrite -1}
		p_cast2 {Type I LastRead 0 FirstWrite -1}
		p_cast3 {Type I LastRead 0 FirstWrite -1}
		p_cast4 {Type I LastRead 0 FirstWrite -1}
		p_cast {Type I LastRead 0 FirstWrite -1}
		in_mat_data24 {Type I LastRead 1 FirstWrite -1}
		cmp_i_i131_i {Type I LastRead 0 FirstWrite -1}
		out_mat_data25 {Type O LastRead -1 FirstWrite 16}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}
	xfMat2AXIvideo_8_0_128_128_1_1_s {
		p_read {Type I LastRead 0 FirstWrite -1}
		p_read1 {Type I LastRead 0 FirstWrite -1}
		out_mat_data25 {Type I LastRead 1 FirstWrite -1}
		dst_V_data_V {Type O LastRead -1 FirstWrite 1}
		dst_V_keep_V {Type O LastRead -1 FirstWrite 1}
		dst_V_strb_V {Type O LastRead -1 FirstWrite 1}
		dst_V_user_V {Type O LastRead -1 FirstWrite 1}
		dst_V_last_V {Type O LastRead -1 FirstWrite 1}
		dst_V_id_V {Type O LastRead -1 FirstWrite 1}
		dst_V_dest_V {Type O LastRead -1 FirstWrite 1}}
	xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi {
		sof {Type I LastRead 0 FirstWrite -1}
		p_read1 {Type I LastRead 0 FirstWrite -1}
		sub {Type I LastRead 0 FirstWrite -1}
		out_mat_data25 {Type I LastRead 1 FirstWrite -1}
		dst_V_data_V {Type O LastRead -1 FirstWrite 1}
		dst_V_keep_V {Type O LastRead -1 FirstWrite 1}
		dst_V_strb_V {Type O LastRead -1 FirstWrite 1}
		dst_V_user_V {Type O LastRead -1 FirstWrite 1}
		dst_V_last_V {Type O LastRead -1 FirstWrite 1}
		dst_V_id_V {Type O LastRead -1 FirstWrite 1}
		dst_V_dest_V {Type O LastRead -1 FirstWrite 1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "53", "Max" : "19747"}
	, {"Name" : "Interval", "Min" : "52", "Max" : "19746"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	src_V_data_V { axis {  { src_TDATA in_data 0 8 } } }
	src_V_keep_V { axis {  { src_TKEEP in_data 0 1 } } }
	src_V_strb_V { axis {  { src_TSTRB in_data 0 1 } } }
	src_V_user_V { axis {  { src_TUSER in_data 0 1 } } }
	src_V_last_V { axis {  { src_TLAST in_data 0 1 } } }
	src_V_id_V { axis {  { src_TID in_data 0 1 } } }
	src_V_dest_V { axis {  { src_TDEST in_data 0 1 }  { src_TVALID in_vld 0 1 }  { src_TREADY in_acc 1 1 } } }
	dst_V_data_V { axis {  { dst_TDATA out_data 1 8 } } }
	dst_V_keep_V { axis {  { dst_TKEEP out_data 1 1 } } }
	dst_V_strb_V { axis {  { dst_TSTRB out_data 1 1 } } }
	dst_V_user_V { axis {  { dst_TUSER out_data 1 1 } } }
	dst_V_last_V { axis {  { dst_TLAST out_data 1 1 } } }
	dst_V_id_V { axis {  { dst_TID out_data 1 1 } } }
	dst_V_dest_V { axis {  { dst_TDEST out_data 1 1 }  { dst_TVALID out_vld 1 1 }  { dst_TREADY out_acc 0 1 } } }
}

set maxi_interface_dict [dict create]

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
