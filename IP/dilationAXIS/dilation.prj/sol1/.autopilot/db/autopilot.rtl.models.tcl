set SynModuleInfo {
  {SRCNAME Block_entry1_proc MODELNAME Block_entry1_proc RTLNAME dilation_accel_Block_entry1_proc}
  {SRCNAME {AXIvideo2xfMat<8, 0, 128, 128, 1, 1>_Pipeline_loop_start_hunt} MODELNAME AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt RTLNAME dilation_accel_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_start_hunt
    SUBMODULES {
      {MODELNAME dilation_accel_flow_control_loop_pipe_sequential_init RTLNAME dilation_accel_flow_control_loop_pipe_sequential_init BINDTYPE interface TYPE internal_upc_flow_control INSTNAME dilation_accel_flow_control_loop_pipe_sequential_init_U}
    }
  }
  {SRCNAME {AXIvideo2xfMat<8, 0, 128, 128, 1, 1>_Pipeline_loop_col_zxi2mat} MODELNAME AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat RTLNAME dilation_accel_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_col_zxi2mat}
  {SRCNAME {AXIvideo2xfMat<8, 0, 128, 128, 1, 1>_Pipeline_loop_last_hunt} MODELNAME AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt RTLNAME dilation_accel_AXIvideo2xfMat_8_0_128_128_1_1_Pipeline_loop_last_hunt}
  {SRCNAME {AXIvideo2xfMat<8, 0, 128, 128, 1, 1>} MODELNAME AXIvideo2xfMat_8_0_128_128_1_1_s RTLNAME dilation_accel_AXIvideo2xfMat_8_0_128_128_1_1_s
    SUBMODULES {
      {MODELNAME dilation_accel_regslice_both RTLNAME dilation_accel_regslice_both BINDTYPE interface TYPE interface_regslice INSTNAME dilation_accel_regslice_both_U}
    }
  }
  {SRCNAME {xfdilate<128, 128, 1, 0, 1, 1, 1, 0, 130, 5, 5>_Pipeline_VITIS_LOOP_279_1} MODELNAME xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1 RTLNAME dilation_accel_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1}
  {SRCNAME {xfdilate<128, 128, 1, 0, 1, 1, 1, 0, 130, 5, 5>_Pipeline_VITIS_LOOP_294_2} MODELNAME xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2 RTLNAME dilation_accel_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2}
  {SRCNAME xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3 MODELNAME xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3 RTLNAME dilation_accel_xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3}
  {SRCNAME {xfExtractPixels<1, 1, 0>} MODELNAME xfExtractPixels_1_1_0_s RTLNAME dilation_accel_xfExtractPixels_1_1_0_s}
  {SRCNAME {xfdilate<128, 128, 1, 0, 1, 1, 1, 0, 130, 5, 5>_Pipeline_Col_Loop} MODELNAME xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop RTLNAME dilation_accel_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop
    SUBMODULES {
      {MODELNAME dilation_accel_mux_53_8_1_1 RTLNAME dilation_accel_mux_53_8_1_1 BINDTYPE op TYPE mux IMPL auto LATENCY 0 ALLOW_PRAGMA 1}
    }
  }
  {SRCNAME {xfdilate<128, 128, 1, 0, 1, 1, 1, 0, 130, 5, 5>} MODELNAME xfdilate_128_128_1_0_1_1_1_0_130_5_5_s RTLNAME dilation_accel_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s
    SUBMODULES {
      {MODELNAME dilation_accel_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_buf_V_RAM_S2P_BRAM_1R1W RTLNAME dilation_accel_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_buf_V_RAM_S2P_BRAM_1R1W BINDTYPE storage TYPE ram_s2p IMPL bram LATENCY 2 ALLOW_PRAGMA 1}
    }
  }
  {SRCNAME {dilate<0, 0, 128, 128, 2, 5, 5, 1, 1, 1, 1>} MODELNAME dilate_0_0_128_128_2_5_5_1_1_1_1_s RTLNAME dilation_accel_dilate_0_0_128_128_2_5_5_1_1_1_1_s}
  {SRCNAME {xfMat2AXIvideo<8, 0, 128, 128, 1, 1>_Pipeline_loop_col_mat2axi} MODELNAME xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi RTLNAME dilation_accel_xfMat2AXIvideo_8_0_128_128_1_1_Pipeline_loop_col_mat2axi}
  {SRCNAME {xfMat2AXIvideo<8, 0, 128, 128, 1, 1>} MODELNAME xfMat2AXIvideo_8_0_128_128_1_1_s RTLNAME dilation_accel_xfMat2AXIvideo_8_0_128_128_1_1_s}
  {SRCNAME dilation_accel MODELNAME dilation_accel RTLNAME dilation_accel IS_TOP 1
    SUBMODULES {
      {MODELNAME dilation_accel_fifo_w32_d4_S RTLNAME dilation_accel_fifo_w32_d4_S BINDTYPE storage TYPE fifo IMPL srl ALLOW_PRAGMA 1 INSTNAME out_mat_rows_channel_U}
      {MODELNAME dilation_accel_fifo_w32_d4_S RTLNAME dilation_accel_fifo_w32_d4_S BINDTYPE storage TYPE fifo IMPL srl ALLOW_PRAGMA 1 INSTNAME out_mat_cols_channel_U}
      {MODELNAME dilation_accel_fifo_w32_d2_S RTLNAME dilation_accel_fifo_w32_d2_S BINDTYPE storage TYPE fifo IMPL srl ALLOW_PRAGMA 1 INSTNAME in_mat_rows_c9_channel_U}
      {MODELNAME dilation_accel_fifo_w32_d2_S RTLNAME dilation_accel_fifo_w32_d2_S BINDTYPE storage TYPE fifo IMPL srl ALLOW_PRAGMA 1 INSTNAME in_mat_cols_c10_channel_U}
      {MODELNAME dilation_accel_fifo_w8_d1_S RTLNAME dilation_accel_fifo_w8_d1_S BINDTYPE storage TYPE fifo IMPL srl ALLOW_PRAGMA 1 INSTNAME in_mat_data_U}
      {MODELNAME dilation_accel_fifo_w32_d2_S RTLNAME dilation_accel_fifo_w32_d2_S BINDTYPE storage TYPE fifo IMPL srl ALLOW_PRAGMA 1 INSTNAME in_mat_rows_c_U}
      {MODELNAME dilation_accel_fifo_w32_d2_S RTLNAME dilation_accel_fifo_w32_d2_S BINDTYPE storage TYPE fifo IMPL srl ALLOW_PRAGMA 1 INSTNAME in_mat_cols_c_U}
      {MODELNAME dilation_accel_fifo_w8_d1_S RTLNAME dilation_accel_fifo_w8_d1_S BINDTYPE storage TYPE fifo IMPL srl ALLOW_PRAGMA 1 INSTNAME out_mat_data_U}
      {MODELNAME dilation_accel_start_for_dilate_0_0_128_128_2_5_5_1_1_1_1_U0 RTLNAME dilation_accel_start_for_dilate_0_0_128_128_2_5_5_1_1_1_1_U0 BINDTYPE storage TYPE fifo IMPL srl ALLOW_PRAGMA 1 INSTNAME start_for_dilate_0_0_128_128_2_5_5_1_1_1_1_U0_U}
      {MODELNAME dilation_accel_control_s_axi RTLNAME dilation_accel_control_s_axi BINDTYPE interface TYPE interface_s_axilite}
      {MODELNAME dilation_accel_control_r_s_axi RTLNAME dilation_accel_control_r_s_axi BINDTYPE interface TYPE interface_s_axilite}
    }
  }
}
