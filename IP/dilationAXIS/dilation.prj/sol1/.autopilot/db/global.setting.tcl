
set TopModule "dilation_accel"
set ClockPeriod 3.3
set ClockList ap_clk
set HasVivadoClockPeriod 0
set CombLogicFlag 0
set PipelineFlag 1
set DataflowTaskPipelineFlag 1
set TrivialPipelineFlag 0
set noPortSwitchingFlag 0
set FloatingPointFlag 0
set FftOrFirFlag 0
set NbRWValue 0
set intNbAccess 0
set NewDSPMapping 1
set HasDSPModule 0
set ResetLevelFlag 0
set ResetStyle control
set ResetSyncFlag 1
set ResetRegisterFlag 0
set ResetVariableFlag 0
set ResetRegisterNum 0
set FsmEncStyle onehot
set MaxFanout 0
set RtlPrefix {}
set RtlSubPrefix dilation_accel_
set ExtraCCFlags {}
set ExtraCLdFlags {}
set SynCheckOptions {}
set PresynOptions {}
set PreprocOptions {}
set SchedOptions {}
set BindOptions {}
set RtlGenOptions {}
set RtlWriterOptions {}
set CbcGenFlag {}
set CasGenFlag {}
set CasMonitorFlag {}
set AutoSimOptions {}
set ExportMCPathFlag 0
set SCTraceFileName mytrace
set SCTraceFileFormat vcd
set SCTraceOption all
set TargetInfo xczu9eg:-ffvb1156:-2-i
set SourceFiles {sc {} c /home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/dilationAXIS/xf_dilation_accel.cpp}
set SourceFlags {sc {} c {{ -I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/dilationAXIS/build  -I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/include  -I../.././.  -D__SDSVHLS__ -std=c++0x} {-I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/dilationAXIS/build -I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/include -I./. -D__SDSVHLS__ -std=c++0x}}}
set DirectiveFile /opt/users/dhimiter.qendri/Vitis_Libraries/vision/L1/examples/dilationAXIS/dilation.prj/sol1/sol1.directive
set TBFiles {verilog /home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/dilationAXIS/xf_dilation_tb.cpp bc /home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/dilationAXIS/xf_dilation_tb.cpp sc /home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/dilationAXIS/xf_dilation_tb.cpp vhdl /home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/dilationAXIS/xf_dilation_tb.cpp c {} cas /home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/dilationAXIS/xf_dilation_tb.cpp}
set SpecLanguage C
set TVInFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TVOutFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TBTops {verilog {} bc {} sc {} vhdl {} c {} cas {}}
set TBInstNames {verilog {} bc {} sc {} vhdl {} c {} cas {}}
set XDCFiles {}
set ExtraGlobalOptions {"area_timing" 1 "clock_gate" 1 "impl_flow" map "power_gate" 0}
set TBTVFileNotFound {}
set AppFile ../hls.app
set ApsFile sol1.aps
set AvePath ../..
set DefaultPlatform DefaultPlatform
set multiClockList {}
set SCPortClockMap {}
set intNbAccess 0
set PlatformFiles {{DefaultPlatform {xilinx/zynquplus/zynquplus}}}
set HPFPO 0
