set moduleName dilate_0_0_128_128_2_5_5_1_1_1_1_s
set isTopModule 0
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set hasInterrupt 0
set C_modelName {dilate<0, 0, 128, 128, 2, 5, 5, 1, 1, 1, 1>}
set C_modelType { void 0 }
set C_modelArgList {
	{ p_src_rows int 32 regular {fifo 0}  }
	{ p_src_cols int 32 regular {fifo 0}  }
	{ in_mat_data24 int 8 regular {fifo 0 volatile }  }
	{ out_mat_data25 int 8 regular {fifo 1 volatile }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "p_src_rows", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "p_src_cols", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "in_mat_data24", "interface" : "fifo", "bitwidth" : 8, "direction" : "READONLY"} , 
 	{ "Name" : "out_mat_data25", "interface" : "fifo", "bitwidth" : 8, "direction" : "WRITEONLY"} ]}
# RTL Port declarations: 
set portNum 27
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ p_src_rows_dout sc_in sc_lv 32 signal 0 } 
	{ p_src_rows_num_data_valid sc_in sc_lv 2 signal 0 } 
	{ p_src_rows_fifo_cap sc_in sc_lv 2 signal 0 } 
	{ p_src_rows_empty_n sc_in sc_logic 1 signal 0 } 
	{ p_src_rows_read sc_out sc_logic 1 signal 0 } 
	{ p_src_cols_dout sc_in sc_lv 32 signal 1 } 
	{ p_src_cols_num_data_valid sc_in sc_lv 2 signal 1 } 
	{ p_src_cols_fifo_cap sc_in sc_lv 2 signal 1 } 
	{ p_src_cols_empty_n sc_in sc_logic 1 signal 1 } 
	{ p_src_cols_read sc_out sc_logic 1 signal 1 } 
	{ in_mat_data24_dout sc_in sc_lv 8 signal 2 } 
	{ in_mat_data24_num_data_valid sc_in sc_lv 2 signal 2 } 
	{ in_mat_data24_fifo_cap sc_in sc_lv 2 signal 2 } 
	{ in_mat_data24_empty_n sc_in sc_logic 1 signal 2 } 
	{ in_mat_data24_read sc_out sc_logic 1 signal 2 } 
	{ out_mat_data25_din sc_out sc_lv 8 signal 3 } 
	{ out_mat_data25_num_data_valid sc_in sc_lv 2 signal 3 } 
	{ out_mat_data25_fifo_cap sc_in sc_lv 2 signal 3 } 
	{ out_mat_data25_full_n sc_in sc_logic 1 signal 3 } 
	{ out_mat_data25_write sc_out sc_logic 1 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "p_src_rows_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "p_src_rows", "role": "dout" }} , 
 	{ "name": "p_src_rows_num_data_valid", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "p_src_rows", "role": "num_data_valid" }} , 
 	{ "name": "p_src_rows_fifo_cap", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "p_src_rows", "role": "fifo_cap" }} , 
 	{ "name": "p_src_rows_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "p_src_rows", "role": "empty_n" }} , 
 	{ "name": "p_src_rows_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "p_src_rows", "role": "read" }} , 
 	{ "name": "p_src_cols_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "p_src_cols", "role": "dout" }} , 
 	{ "name": "p_src_cols_num_data_valid", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "p_src_cols", "role": "num_data_valid" }} , 
 	{ "name": "p_src_cols_fifo_cap", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "p_src_cols", "role": "fifo_cap" }} , 
 	{ "name": "p_src_cols_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "p_src_cols", "role": "empty_n" }} , 
 	{ "name": "p_src_cols_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "p_src_cols", "role": "read" }} , 
 	{ "name": "in_mat_data24_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "in_mat_data24", "role": "dout" }} , 
 	{ "name": "in_mat_data24_num_data_valid", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "in_mat_data24", "role": "num_data_valid" }} , 
 	{ "name": "in_mat_data24_fifo_cap", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "in_mat_data24", "role": "fifo_cap" }} , 
 	{ "name": "in_mat_data24_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "in_mat_data24", "role": "empty_n" }} , 
 	{ "name": "in_mat_data24_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "in_mat_data24", "role": "read" }} , 
 	{ "name": "out_mat_data25_din", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "out_mat_data25", "role": "din" }} , 
 	{ "name": "out_mat_data25_num_data_valid", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "out_mat_data25", "role": "num_data_valid" }} , 
 	{ "name": "out_mat_data25_fifo_cap", "direction": "in", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "out_mat_data25", "role": "fifo_cap" }} , 
 	{ "name": "out_mat_data25_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "out_mat_data25", "role": "full_n" }} , 
 	{ "name": "out_mat_data25_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "out_mat_data25", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "dilate_0_0_128_128_2_5_5_1_1_1_1_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "51", "EstimateLatencyMax" : "19745",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_src_rows", "Type" : "Fifo", "Direction" : "I", "DependentProc" : ["0"], "DependentChan" : "0", "DependentChanDepth" : "2", "DependentChanType" : "2",
				"BlockSignal" : [
					{"Name" : "p_src_rows_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "p_src_cols", "Type" : "Fifo", "Direction" : "I", "DependentProc" : ["0"], "DependentChan" : "0", "DependentChanDepth" : "2", "DependentChanType" : "2",
				"BlockSignal" : [
					{"Name" : "p_src_cols_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "I", "DependentProc" : ["0"], "DependentChan" : "0", "DependentChanDepth" : "1", "DependentChanType" : "0",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44", "Port" : "in_mat_data24", "Inst_start_state" : "1", "Inst_end_state" : "2"}]},
			{"Name" : "out_mat_data25", "Type" : "Fifo", "Direction" : "O", "DependentProc" : ["0"], "DependentChan" : "0", "DependentChanDepth" : "1", "DependentChanType" : "0",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44", "Port" : "out_mat_data25", "Inst_start_state" : "1", "Inst_end_state" : "2"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44", "Parent" : "0", "Child" : ["2", "3", "4", "5", "6", "7", "9", "11", "13"],
		"CDFG" : "xfdilate_128_128_1_0_1_1_1_0_130_5_5_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "50", "EstimateLatencyMax" : "19744",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "9", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2_fu_163", "Port" : "in_mat_data24", "Inst_start_state" : "4", "Inst_end_state" : "5"},
					{"ID" : "13", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181", "Port" : "in_mat_data24", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "out_mat_data25", "Type" : "Fifo", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "13", "SubInstance" : "grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181", "Port" : "out_mat_data25", "Inst_start_state" : "8", "Inst_end_state" : "9"}]},
			{"Name" : "img_height", "Type" : "None", "Direction" : "I"},
			{"Name" : "img_width", "Type" : "None", "Direction" : "I"}],
		"Loop" : [
			{"Name" : "read_lines", "PipelineType" : "no",
				"LoopDec" : {"FSMBitwidth" : "9", "FirstState" : "ap_ST_fsm_state4", "LastState" : ["ap_ST_fsm_state5"], "QuitState" : ["ap_ST_fsm_state4"], "PreState" : ["ap_ST_fsm_state3"], "PostState" : ["ap_ST_fsm_state6"], "OneDepthLoop" : "0", "OneStateBlock": ""}},
			{"Name" : "Row_Loop", "PipelineType" : "no",
				"LoopDec" : {"FSMBitwidth" : "9", "FirstState" : "ap_ST_fsm_state7", "LastState" : ["ap_ST_fsm_state9"], "QuitState" : ["ap_ST_fsm_state7"], "PreState" : ["ap_ST_fsm_state6"], "PostState" : ["ap_ST_fsm_state1"], "OneDepthLoop" : "0", "OneStateBlock": ""}}]},
	{"ID" : "2", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_U", "Parent" : "1"},
	{"ID" : "3", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_1_U", "Parent" : "1"},
	{"ID" : "4", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_2_U", "Parent" : "1"},
	{"ID" : "5", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_3_U", "Parent" : "1"},
	{"ID" : "6", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.buf_V_4_U", "Parent" : "1"},
	{"ID" : "7", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1_fu_154", "Parent" : "1", "Child" : ["8"],
		"CDFG" : "xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "7", "EstimateLatencyMax" : "7",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "row_ind_V_4_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "row_ind_V_3_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "row_ind_V_2_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "row_ind_V_1_out", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "row_ind_V_out", "Type" : "Vld", "Direction" : "O"}],
		"Loop" : [
			{"Name" : "VITIS_LOOP_279_1", "PipelineType" : "NotSupport"}]},
	{"ID" : "8", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1_fu_154.flow_control_loop_pipe_sequential_init_U", "Parent" : "7"},
	{"ID" : "9", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2_fu_163", "Parent" : "1", "Child" : ["10"],
		"CDFG" : "xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "133",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "img_width", "Type" : "None", "Direction" : "I"},
			{"Name" : "buf_V_3", "Type" : "Memory", "Direction" : "O"},
			{"Name" : "buf_V_2", "Type" : "Memory", "Direction" : "O"},
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "in_mat_data24_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cond21", "Type" : "None", "Direction" : "I"}],
		"Loop" : [
			{"Name" : "VITIS_LOOP_294_2", "PipelineType" : "UPC",
				"LoopDec" : {"FSMBitwidth" : "1", "FirstState" : "ap_ST_fsm_pp0_stage0", "FirstStateIter" : "ap_enable_reg_pp0_iter0", "FirstStateBlock" : "ap_block_pp0_stage0_subdone", "LastState" : "ap_ST_fsm_pp0_stage0", "LastStateIter" : "ap_enable_reg_pp0_iter2", "LastStateBlock" : "ap_block_pp0_stage0_subdone", "QuitState" : "ap_ST_fsm_pp0_stage0", "QuitStateIter" : "ap_enable_reg_pp0_iter2", "QuitStateBlock" : "ap_block_pp0_stage0_subdone", "OneDepthLoop" : "0", "has_ap_ctrl" : "1", "has_continue" : "0"}}]},
	{"ID" : "10", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2_fu_163.flow_control_loop_pipe_sequential_init_U", "Parent" : "9"},
	{"ID" : "11", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3_fu_173", "Parent" : "1", "Child" : ["12"],
		"CDFG" : "xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "4", "EstimateLatencyMax" : "262",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "img_width", "Type" : "None", "Direction" : "I"},
			{"Name" : "zext_ln1027_6", "Type" : "None", "Direction" : "I"},
			{"Name" : "buf_V", "Type" : "Memory", "Direction" : "O"},
			{"Name" : "buf_V_1", "Type" : "Memory", "Direction" : "O"}],
		"Loop" : [
			{"Name" : "init_boarder_VITIS_LOOP_311_3", "PipelineType" : "UPC",
				"LoopDec" : {"FSMBitwidth" : "1", "FirstState" : "ap_ST_fsm_pp0_stage0", "FirstStateIter" : "ap_enable_reg_pp0_iter0", "FirstStateBlock" : "ap_block_pp0_stage0_subdone", "LastState" : "ap_ST_fsm_pp0_stage0", "LastStateIter" : "ap_enable_reg_pp0_iter1", "LastStateBlock" : "ap_block_pp0_stage0_subdone", "QuitState" : "ap_ST_fsm_pp0_stage0", "QuitStateIter" : "ap_enable_reg_pp0_iter0", "QuitStateBlock" : "ap_block_pp0_stage0_subdone", "OneDepthLoop" : "0", "has_ap_ctrl" : "1", "has_continue" : "0"}}]},
	{"ID" : "12", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3_fu_173.flow_control_loop_pipe_sequential_init_U", "Parent" : "11"},
	{"ID" : "13", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181", "Parent" : "1", "Child" : ["14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"],
		"CDFG" : "xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "19", "EstimateLatencyMax" : "147",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "op2_assign_1", "Type" : "None", "Direction" : "I"},
			{"Name" : "buf_V_4", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "buf_V_3", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "buf_V_2", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "buf_V_1", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "buf_V", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "img_width", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast1", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast2", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast3", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast4", "Type" : "None", "Direction" : "I"},
			{"Name" : "p_cast", "Type" : "None", "Direction" : "I"},
			{"Name" : "in_mat_data24", "Type" : "Fifo", "Direction" : "I",
				"BlockSignal" : [
					{"Name" : "in_mat_data24_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "cmp_i_i131_i", "Type" : "None", "Direction" : "I"},
			{"Name" : "out_mat_data25", "Type" : "Fifo", "Direction" : "O",
				"BlockSignal" : [
					{"Name" : "out_mat_data25_blk_n", "Type" : "RtlSignal"}]}],
		"Loop" : [
			{"Name" : "Col_Loop", "PipelineType" : "UPC",
				"LoopDec" : {"FSMBitwidth" : "1", "FirstState" : "ap_ST_fsm_pp0_stage0", "FirstStateIter" : "ap_enable_reg_pp0_iter0", "FirstStateBlock" : "ap_block_pp0_stage0_subdone", "LastState" : "ap_ST_fsm_pp0_stage0", "LastStateIter" : "ap_enable_reg_pp0_iter16", "LastStateBlock" : "ap_block_pp0_stage0_subdone", "QuitState" : "ap_ST_fsm_pp0_stage0", "QuitStateIter" : "ap_enable_reg_pp0_iter16", "QuitStateBlock" : "ap_block_pp0_stage0_subdone", "OneDepthLoop" : "0", "has_ap_ctrl" : "1", "has_continue" : "0"}}]},
	{"ID" : "14", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_xfExtractPixels_1_1_0_s_fu_376", "Parent" : "13",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "15", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_5_xfExtractPixels_1_1_0_s_fu_382", "Parent" : "13",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "16", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_8_xfExtractPixels_1_1_0_s_fu_388", "Parent" : "13",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "17", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_6_xfExtractPixels_1_1_0_s_fu_394", "Parent" : "13",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "18", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.src_buf_temp_copy_extract_V_0_7_xfExtractPixels_1_1_0_s_fu_400", "Parent" : "13",
		"CDFG" : "xfExtractPixels_1_1_0_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0", "real_start" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"IsBlackBox" : "0",
		"Port" : [
			{"Name" : "p_read1", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "19", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U72", "Parent" : "13"},
	{"ID" : "20", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U73", "Parent" : "13"},
	{"ID" : "21", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U74", "Parent" : "13"},
	{"ID" : "22", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U75", "Parent" : "13"},
	{"ID" : "23", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.mux_53_8_1_1_U76", "Parent" : "13"},
	{"ID" : "24", "Level" : "3", "Path" : "`AUTOTB_DUT_INST.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_s_fu_44.grp_xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop_fu_181.flow_control_loop_pipe_sequential_init_U", "Parent" : "13"}]}


set ArgLastReadFirstWriteLatency {
	dilate_0_0_128_128_2_5_5_1_1_1_1_s {
		p_src_rows {Type I LastRead 0 FirstWrite -1}
		p_src_cols {Type I LastRead 0 FirstWrite -1}
		in_mat_data24 {Type I LastRead 1 FirstWrite -1}
		out_mat_data25 {Type O LastRead -1 FirstWrite 16}}
	xfdilate_128_128_1_0_1_1_1_0_130_5_5_s {
		in_mat_data24 {Type I LastRead 1 FirstWrite -1}
		out_mat_data25 {Type O LastRead -1 FirstWrite 16}
		img_height {Type I LastRead 2 FirstWrite -1}
		img_width {Type I LastRead 2 FirstWrite -1}}
	xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_279_1 {
		row_ind_V_4_out {Type O LastRead -1 FirstWrite 0}
		row_ind_V_3_out {Type O LastRead -1 FirstWrite 0}
		row_ind_V_2_out {Type O LastRead -1 FirstWrite 0}
		row_ind_V_1_out {Type O LastRead -1 FirstWrite 0}
		row_ind_V_out {Type O LastRead -1 FirstWrite 0}}
	xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_VITIS_LOOP_294_2 {
		img_width {Type I LastRead 0 FirstWrite -1}
		buf_V_3 {Type O LastRead -1 FirstWrite 2}
		buf_V_2 {Type O LastRead -1 FirstWrite 2}
		in_mat_data24 {Type I LastRead 1 FirstWrite -1}
		cond21 {Type I LastRead 0 FirstWrite -1}}
	xfdilate_Pipeline_init_boarder_VITIS_LOOP_311_3 {
		img_width {Type I LastRead 0 FirstWrite -1}
		zext_ln1027_6 {Type I LastRead 0 FirstWrite -1}
		buf_V {Type O LastRead -1 FirstWrite 1}
		buf_V_1 {Type O LastRead -1 FirstWrite 1}}
	xfdilate_128_128_1_0_1_1_1_0_130_5_5_Pipeline_Col_Loop {
		op2_assign_1 {Type I LastRead 0 FirstWrite -1}
		buf_V_4 {Type IO LastRead 3 FirstWrite 2}
		buf_V_3 {Type IO LastRead 3 FirstWrite 2}
		buf_V_2 {Type IO LastRead 3 FirstWrite 2}
		buf_V_1 {Type IO LastRead 3 FirstWrite 2}
		buf_V {Type IO LastRead 3 FirstWrite 2}
		img_width {Type I LastRead 0 FirstWrite -1}
		p_cast1 {Type I LastRead 0 FirstWrite -1}
		p_cast2 {Type I LastRead 0 FirstWrite -1}
		p_cast3 {Type I LastRead 0 FirstWrite -1}
		p_cast4 {Type I LastRead 0 FirstWrite -1}
		p_cast {Type I LastRead 0 FirstWrite -1}
		in_mat_data24 {Type I LastRead 1 FirstWrite -1}
		cmp_i_i131_i {Type I LastRead 0 FirstWrite -1}
		out_mat_data25 {Type O LastRead -1 FirstWrite 16}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}
	xfExtractPixels_1_1_0_s {
		p_read1 {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "51", "Max" : "19745"}
	, {"Name" : "Interval", "Min" : "51", "Max" : "19745"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	p_src_rows { ap_fifo {  { p_src_rows_dout fifo_port_we 0 32 }  { p_src_rows_num_data_valid fifo_status_num_data_valid 0 2 }  { p_src_rows_fifo_cap fifo_update 0 2 }  { p_src_rows_empty_n fifo_status 0 1 }  { p_src_rows_read fifo_data 1 1 } } }
	p_src_cols { ap_fifo {  { p_src_cols_dout fifo_port_we 0 32 }  { p_src_cols_num_data_valid fifo_status_num_data_valid 0 2 }  { p_src_cols_fifo_cap fifo_update 0 2 }  { p_src_cols_empty_n fifo_status 0 1 }  { p_src_cols_read fifo_data 1 1 } } }
	in_mat_data24 { ap_fifo {  { in_mat_data24_dout fifo_port_we 0 8 }  { in_mat_data24_num_data_valid fifo_status_num_data_valid 0 2 }  { in_mat_data24_fifo_cap fifo_update 0 2 }  { in_mat_data24_empty_n fifo_status 0 1 }  { in_mat_data24_read fifo_data 1 1 } } }
	out_mat_data25 { ap_fifo {  { out_mat_data25_din fifo_port_we 1 8 }  { out_mat_data25_num_data_valid fifo_status_num_data_valid 0 2 }  { out_mat_data25_fifo_cap fifo_update 0 2 }  { out_mat_data25_full_n fifo_status 0 1 }  { out_mat_data25_write fifo_data 1 1 } } }
}
