#ifndef _XF_DILATION_CONFIG_H_
#define _XF_DILATION_CONFIG_H_

#include "hls_stream.h"
#include "ap_int.h"
#include "common/xf_common.hpp"
#include "common/xf_utility.hpp"
#include "imgproc/xf_dilation.hpp"
#include "xf_config_params.h"
#include "common/xf_infra.hpp"

/* config width and height */
#define WIDTH 128
#define HEIGHT 128

#define INPUT_PTR_WIDTH 128
#define OUTPUT_PTR_WIDTH 128

/*  define the input and output types  */
#if SPC
#define NPC_T XF_NPPC1
#else // MPC
#define NPC_T XF_NPPC8
#endif

#if GRAY
#define TYPE XF_8UC1
#define CH_TYPE XF_GRAY
#else // RGB
#define TYPE XF_8UC3
#define CH_TYPE XF_RGB
#endif


void dilation_accel(hls::stream<ap_axiu<8, 1, 1, 1> >& _src, hls::stream<ap_axiu<8, 1, 1, 1> >& _dst, 
                    //unsigned char* kernel,
                    int height,
                    int width);
                  
#endif // _XF_DILATION_CONFIG_H_
