#define MPC 0 // Multiple Pixels per Clock operation
#define SPC 1 // Single Pixel per Clock operation

#define GRAY 1

#define FILTER_SIZE 5

#define KERNEL_SHAPE XF_SHAPE_CROSS

#define ITERATIONS 1

#define XF_CV_DEPTH_IN_1 1
#define XF_CV_DEPTH_OUT_1 1
