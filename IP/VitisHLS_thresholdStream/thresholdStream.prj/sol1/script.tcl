############################################################
## This file is generated automatically by Vitis HLS.
## Please DO NOT edit it.
## Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
############################################################
open_project thresholdStream.prj
set_top threshold_accel
add_files ../../../../../../../../home/dhimiter.qendri/work/Vitis_Libraries/vision/L1/examples/thresholdStream/xf_thresh_accel.cpp -cflags "-I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/thresholdStream/build -I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/include -I./. -D__SDSVHLS__ -std=c++0x" -csimflags "-I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/thresholdStream/build -I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/include -I./. -D__SDSVHLS__ -std=c++0x"
add_files -tb ../../../../../../../../home/dhimiter.qendri/work/Vitis_Libraries/vision/L1/examples/thresholdStream/xf_thresh_tb.cpp -cflags "-I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/thresholdStream/build -I/usr/local/include/opencv4 -I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/include -I. -I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/examples/thresholdStream/build -I/home/dhimiter.qendri/work/Vitis_Libraries/vision//L1/include -I. -D__SDSVHLS__ -std=c++0x -D__SDSVHLS__ -std=c++0x -Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
open_solution "sol1" -flow_target vivado
set_part {xczu9eg-ffvb1156-2-i}
create_clock -period 3.3 -name default
config_interface -m_axi_latency 0
#source "./thresholdStream.prj/sol1/directives.tcl"
csim_design -ldflags {-L /usr/local/lib -lopencv_imgcodecs -lopencv_imgproc -lopencv_core -lopencv_highgui -lopencv_flann -lopencv_features2d} -argv {/home/dhimiter.qendri/work/Vitis_Libraries/vision//data/ref_Y.png} -clean
csynth_design
cosim_design -ldflags {-L /usr/local/lib -lopencv_imgcodecs -lopencv_imgproc -lopencv_core -lopencv_highgui -lopencv_flann -lopencv_features2d}
export_design -format ip_catalog
