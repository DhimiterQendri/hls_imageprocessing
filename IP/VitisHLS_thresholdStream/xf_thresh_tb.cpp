#include "common/xf_headers.hpp"
#include "xf_thresh_config.h"
#include "common/xf_axi.hpp"

using namespace std;

#define _W 8

int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Invalid Number of Arguments!\nUsage: <executable> <image>\n");
        return -1;
    }


    cv::Mat in_img, ocv_ref, in_gray, diff;
     
    in_img = cv::imread(argv[1], 0);
    if (in_img.data == NULL) {
        fprintf(stderr, "Cannot open image at %s\n", argv[1]);
        return 0;
    }

    cv::imwrite("input.png", in_img);
    
    int rows = in_img.rows;
    int cols = in_img.cols;
    cv::Mat out_img(rows, cols, CV_8UC1);

    ocv_ref.create(in_img.rows, in_img.cols, in_img.depth());
    out_img.create(in_img.rows, in_img.cols, in_img.depth());
    diff.create(in_img.rows, in_img.cols, in_img.depth());
    
    
    unsigned char maxval = 255;
    unsigned char thresh = 100;

    cv::threshold(in_img, ocv_ref, thresh, maxval, THRESH_TYPE);
    
    
    // convert input to axiStream
    hls::stream<ap_axiu<_W, 1, 1, 1> > _src;
    xf::cv::cvMat2AXIvideoxf<XF_NPPC1, _W>(in_img, _src);

    // output axiStream
    hls::stream<ap_axiu<_W, 1, 1, 1> > _dst;

    // Launch the kernel
    threshold_accel(_src, _dst,  thresh, maxval,rows, cols);

    xf::cv::AXIvideo2cvMatxf<XF_NPPC1>(_dst, out_img);

    // Write output image
    cv::imwrite("output.png", out_img);

    /**** validation ****/
    // diff
    diff.create(ocv_ref.rows, ocv_ref.cols, CV_8UC1);
    // Compute absolute difference image
    cv::absdiff(ocv_ref, out_img, diff);
    
    // Save the difference image
    imwrite("diff.png", diff);
    
    imwrite("error.png", diff); // Save the difference image for debugging purpose
    float err_per;
    xf::cv::analyzeDiff(diff, 0, err_per);
    if (err_per > 0.0f) {
        fprintf(stderr, "ERROR: Test Failed.\n ");
        return 1;
    } else
        std::cout << "Test Passed " << std::endl;
    /**** end of validation ****/

    return 0;
}
