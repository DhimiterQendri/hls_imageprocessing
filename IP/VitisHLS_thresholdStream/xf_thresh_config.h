#ifndef _XF_AXICONV_CONFIG_H_
#define _XF_AXICONV_CONFIG_H_

#include "common/xf_common.hpp"
#include "common/xf_utility.hpp"
#include "common/xf_infra.hpp"
#include "imgproc/xf_threshold.hpp"
#include "xf_config_params.h"


/* config width and height */
#define XF_HEIGHT 1080
#define XF_WIDTH 1920

#define IN_TYPE ap_uint<8>
#define OUT_TYPE ap_uint<8>

#define INPUT_PTR_WIDTH 32
#define OUTPUT_PTR_WIDTH 32

typedef ap_uint<8> ap_uint8_t;
typedef ap_uint<64> ap_uint64_t;

/*  set the height and weight  */
#define HEIGHT 1080
#define WIDTH 1920

#if MPC
#define NPIX XF_NPPC8
#endif
#if SPC
#define NPIX XF_NPPC1
#endif


void threshold_accel(hls::stream<ap_axiu<8, 1, 1, 1> >& _src, hls::stream<ap_axiu<8, 1, 1, 1> >& _dst, 
		     unsigned char thresh,
                     unsigned char maxval,
                     int rows, 
                     int cols);

#endif // _XF_AXICONV_CONFIG_H_
