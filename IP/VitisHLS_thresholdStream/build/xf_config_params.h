#define MPC 0 // Multiple Pixels per Clock operation
#define SPC 1 // Single Pixel per Clock operation

#define GRAY 1
#define RGB 0

#define XF_CV_DEPTH_IN_1 2

#define XF_CV_DEPTH_IN 2
#define XF_CV_DEPTH_OUT 2

/*  set the type of thresholding*/
#define THRESH_TYPE XF_THRESHOLD_TYPE_BINARY_INV		//XF_THRESHOLD_TYPE_BINARY  
