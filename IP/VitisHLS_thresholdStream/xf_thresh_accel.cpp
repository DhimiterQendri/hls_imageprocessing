#include "xf_thresh_config.h"

void threshold_accel(hls::stream<ap_axiu<8, 1, 1, 1> >& src, hls::stream<ap_axiu<8, 1, 1, 1> >& dst, 
		     unsigned char thresh,
                     unsigned char maxval,
                     int rows,
                     int cols) {
// clang-format off
    #pragma HLS INTERFACE axis port=src
    #pragma HLS INTERFACE axis port=dst
    
    #pragma HLS INTERFACE s_axilite port=thresh     bundle=control
    #pragma HLS INTERFACE s_axilite port=maxval     bundle=control
    #pragma HLS INTERFACE s_axilite port=rows     bundle=control
    #pragma HLS INTERFACE s_axilite port=cols     bundle=control
    #pragma HLS INTERFACE s_axilite port=return   bundle=control
                
    #pragma HLS INTERFACE s_axilite port=return
    // clang-format on
    
    const int pROWS = HEIGHT;
    const int pCOLS = WIDTH;
    const int pNPC1 = NPIX;

    xf::cv::Mat<XF_8UC1, HEIGHT, WIDTH, NPIX, XF_CV_DEPTH_IN> in_mat(rows, cols);
    // clang-format off
    // clang-format on

    xf::cv::Mat<XF_8UC1, HEIGHT, WIDTH, NPIX, XF_CV_DEPTH_OUT> out_mat(rows, cols);
// clang-format off
// clang-format on

// clang-format off
    #pragma HLS DATAFLOW
    // clang-format on
    

    xf::cv::AXIvideo2xfMat(src, in_mat);
       
    xf::cv::Threshold<THRESH_TYPE, XF_8UC1, HEIGHT, WIDTH, NPIX, XF_CV_DEPTH_IN, XF_CV_DEPTH_OUT>(in_mat, out_mat, thresh, maxval);
        
    xf::cv::xfMat2AXIvideo(out_mat, dst);

    return;
}
