#define MPC 0 // Multiple Pixels per Clock operation
#define SPC 1 // Single Pixel per Clock operation

// port widths
#define INPUT_PTR_WIDTH 128
#define OUTPUT_PTR_WIDTH 128

// For Nearest Neighbor & Bilinear Interpolation, max down scale factor 2 for all 1-pixel modes, and for upscale in x
// direction
#define MAXDOWNSCALE 4

#define RGB 1
#define GRAY 0
/* Interpolation type*/
#define INTERPOLATION 1
// 0 - Nearest Neighbor Interpolation
// 1 - Bilinear Interpolation
// 2 - AREA Interpolation

/* Input image Dimensions */
#define WIDTH 640  // Maximum Input image width
#define HEIGHT 480 // Maximum Input image height

/* Output image Dimensions */
#define NEWWIDTH 320  // Maximum output image width
#define NEWHEIGHT 240 // Maximum output image height
#define XF_CV_DEPTH_IN 2
#define XF_CV_DEPTH_OUT 2
