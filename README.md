# README #

This repo contains code examples on how to use the Vitis Libraries with Vitis HLS 2021.1

### What is this repository for? ###

Using Vitis Libraries with Vitis HLS 2021.1 on Xilinx FPGA SoC (ZYNQ 7000, ZYNQ MPSOC, Versal).
These libraries constitue a set of synthesizable algorithms with AXI4 or AXI stream interfaces.

### How do I get set up? ###

Refer to the Wiki guide:
https://fidussys.atlassian.net/wiki/spaces/~228458518/pages/edit-v2/2674032667?draftShareId=652cc746-373b-422c-ba9a-7b0fb7887a01

https://fidussys.atlassian.net/wiki/spaces/~228458518/pages/3173842945/Vitis+Libraries+How+To+Guide


1. Recreate Vivado Design
2. Recreate Vitis HLS IP and environment 
3. Use provided Python Jupyter lan notebook to test IP 

![alt text](./ResizeGemm/Vivado.png)



### Who do I talk to? ###

Dhimiter Qendri
dhimiter.qendri@fidus.com